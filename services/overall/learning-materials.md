---
title: Learning Materials
title_image: mountains-nature-arrow-guide-66100.jpg
layout: services/default
author: none
additional_css:
    - title/service-title-buttons.css
redirect_from:
    - services/overall/tutorials
    - services/overall/guidelines
    - guidelines
excerpt:
  "We provide learning materials for all purposes related to software engineering and cloud services usage."
---

{{ page.excerpt }}

## Workshop Material

[Course material](https://gitlab.com/hifis/hifis-workshops) is available for use and reuse under a Creative Commons License.

## Tutorials

<div class="flex-cards">
{%- assign posts = site.categories['Tutorial'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

## Guidelines

<div class="flex-cards">
{%- assign posts = site.categories['Guidelines'] | where: "lang", "en" | sort_natural: "title" -%}
{% for post in posts -%}
{% include cards/post_card.html post=post exclude_date=true %}
{% endfor -%}
</div>

<div class="alert alert-success">
  <h2 id="contact-us"><i class="fas fa-info-circle"></i> Comments or Suggestions?</h2>
  <p>
    Feel free to <a href="{% link contact.md %}">contact us</a>.
  </p>
</div>

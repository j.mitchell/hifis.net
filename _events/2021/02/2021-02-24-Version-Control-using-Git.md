---
title: Version Control using Git
layout: event
organizers:
  - belayneh
lecturers: 
  - belayneh
  - dolling
  - "Fuchs, Hannes"
  - "Lüdtke, Stefan"
  - "Meeßen, Christian"
type:   workshop
start:
    date:   "2021-02-24"
    time:   "09:00"
end:
    date:   "2021-02-24"
    time:   "18:00"
location:
    campus: "Online Event"
fully_booked_out: false
registration_link: https://events.hifis.net/event/49/
registration_period:
    from:   "2021-02-09"
    to:     "2021-02-16"
excerpt:
    "This workshop will teach version control using Git for scientists and PhD students."
---

## Goal

Introduce scientists and PhD students to the use of version control in research environment.

## Content

The workshop covers introductory topics on the use of version control in research, as well as hands on sessions on different functionalities of Git with GitLab. You will find more information on [the workshop page](https://swc-bb.git-pages.gfz-potsdam.de/swc-pages/2021-02-24-virtual/).


## Requirements

Neither prior knowledge nor experience in those tools is needed.
A headset (or at least headphones) is required.

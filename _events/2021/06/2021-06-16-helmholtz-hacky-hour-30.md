---
title: "Helmholtz Hacky Hour #30"
layout: event
organizers:
  - erxleben
type:      hacky-hour
start:
  date:   "2021-06-16"
  time:   "14:00"
end:
  time:   "15:00"
location:
  campus: "Online"
  room:   "<a href=https://meet.gwdg.de/b/max-93j-2ef><i class=\"fas fa-external-link-alt\"></i> meet.gwdg.de</a>"
excerpt:  "<strong>Working effectively with bad or old code</strong> When the code is older than the programmer and your research depends on a cryptic combination of characters…"
---
## Working effectively with bad or old code
The Hacky Hour is intended to discuss and learn about different topics in the context of research software development. During the session, you will meet Helmholtz researchers from different fields and have the opportunity to present your favorite tools and techniques. If you want to join, share your experience or have any question on the topic, let us and others know about it in [the meta pad](https://pad.gwdg.de/0HczFKgqS_C9L1QGzfpbJA#).

How can we deal with legacy code or that one project which was completed in a rush several years ago and never touched again after the paper was accepted? Are there alternatives to panic and/or resignation?

We are looking forward to seeing you!

var spotlights_filter_timeout = null;

function spotlights_filter_input() {
    clearTimeout(spotlights_filter_timeout);
    spotlights_filter_timeout = setTimeout( spotlights_filter, 500);
}

function spotlights_filter() {
    var filters = $('#spotlightsFilterSel').val().split(",").map(function (e) { return e.trim(); });
    var fltstr = $('#spotlightsFilterInput').val();
    var $c = $('section.events-container');
    if (fltstr == "") {
        $c.children('article.list-entry').show();
    } else {
        $c.children('article.list-entry').each(function () {
            for (attr of filters) {
                if (( $(this).attr("data-" + attr) ) &&
                    ( $(this).attr("data-" + attr).toLowerCase().includes(fltstr.toLowerCase()) )
                ) {
                    $(this).show();
                    return;
                }
            }
            $(this).hide();
        });
    }
}


function spotlights_sort() {
    var $c = $('section.events-container');

    var $spotlights = $c.children('article.list-entry');
    var sortList = Array.prototype.sort.bind($spotlights);

    sortList(function ( a, b ) {
        var attr = $('#spotlightsSortSel').val();
        var fact = 1;
        if (attr.slice(0,1) === "-") {
            fact = -1;
            attr = attr.slice(1,attr.length);
        } else if (attr.slice(0,1) === "+") {
            attr = attr.slice(1,attr.length);
        }
        var aAttr = $(a).attr("data-" + attr);
        var bAttr = $(b).attr("data-" + attr);
        console.log(aAttr,bAttr);
        if ((aAttr) && (bAttr)) {
            return aAttr < bAttr ? -fact : (+(aAttr > bAttr)) * fact;
        } else if (aAttr) {
            return fact;
        } else if (bAttr) {
            return -fact;
        } else {
            return 0;
        }
    });
    $c.append($spotlights);
}


function spotlights_add_new_badges( age ) {
    var now = Math.floor( Date.now() / 1000 );
    var $c = $('section.events-container');
    $c.children('article.list-entry').each(function () {
        if ( $(this).attr("data-date_added") ) {
            var d = Math.floor( Date.parse( $(this).attr("data-date_added") ) / 1000 );
            if ( now - d <= age ) {
                $(this).addClass("new");
                $(this).prepend('<div class="badge"><span>New</span></div>');
            }
        }
    });
}


spotlights_filter_input();
spotlights_sort();
spotlights_add_new_badges( 35 * 24 * 60 * 60 );

---
title: "Review of Helmholtz Cloud Service Portfolio completed"
title_image: background-cloud.jpg
date: 2022-03-31
authors:
  - spicker
  - holz
layout: blogpost
categories:
  - News
  - Announcement
  - Cloud Services
excerpt: >
  In January 2022, the first regular Service Portfolio Review started and was completed in March.
---

### First regular Service Portfolio Review completed

**In January 2022, the first regular Service Portfolio Review started and was completed in March.**

More than a year has passed since the initial Helmholtz Cloud Service Portfolio was
[announced in October 2020]({% post_url 2020/10/2020-10-13-initial-service-portfolio %}).
In the meantime, a lot has happened: 22 services can already be used via the [Helmholtz Cloud Portal](https://helmholtz.cloud/services); the processes around the Service Portfolio were worked out and documented in the [Process Framework](https://hifis.net/doc/process-framework/Chapter-Overview/) and thus we agreed on the rules along the whole service lifecycle.

Now it was time to conduct the first regular review of the Helmholtz Cloud Service Portfolio and put our [Service Portfolio Review process](https://hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.4_Processes-regarding-the-Service-Portfolio/3.4.6_Review-Process-for-the-Service-Portfolio/) into practice.

During the review of the services in the portfolio, the following points are checked:
* Services in the service catalogue (=listed in the Helmholtz Cloud Portal).
  * Each service still meets the exclusion criteria (=subset of the service selection criteria).
* Serviced in the service pipeline (=upcoming services)
  * Each service still meets the exclusion criteria 
  * Re-evaluation of the weighting criteria 
* Service Selection criteria
  * Check whether criteria are still fitting and add/change/delete criteria if necessary
* Service Portfolio processes
  * Check whether Service Portfolio processes are still fitting and add/change processes and Process Framework if necessary


### The process was carried out smoothly
For each service in the service pipeline and in the service catalogue, the cloud service providers reviewed a set of service information. Due to the adjusted service selection criteria since the initial service selection, service providers added information that was not yet given and adapted information that changed in the meantime.

### The results were evaluated and incorporated
The following changes have come out of the review:
- 7 services are removed from the list of upcoming services (Service Pipeline) as the service providers have withdrawn their service offering. The [current Service Portfolio can be checked here](https://hifis.net/doc/service-portfolio/service-portfolio-management/current-services-in-portfolio/).
- Changes to the wording of the [service selection criteria](https://hifis.net/doc/process-framework/0_Corresponding-files/HIFIS-Cloud-SP_Service-Selection-Criteria-v2.01.pdf), the [Application Form in Plony](https://plony.helmholtz-berlin.de) and the [Service Canvas](https://hifis.net/doc/process-framework/0_Corresponding-files/HIFIS-Cloud-SP_Service-Canvas-Form_v1.5.pdf) (Excel form, soon also available in Plony) have been made on the basis of experience gained so far.
- The wording of the [service type "HIFIS Basis Service"](https://hifis.net/doc/process-framework/3_Service-Portfolio-Management/3.2_Service-Type-Definitions/3.2.0_Service-Type-Definitions/) was changed to "Helmholtz Cloud core service" due to interfaces to NFDI and the different definition of a basic service there.
For more details please check the [review documentation](https://hifis.net/doc/service-portfolio/service-portfolio-management/service-portfolio-review-documentations/).

### The Portfolio is kept up to date
The next regular review is planned for October 2022, so that both the feedback from the HIFIS evaluation in September can be incorporated and the review results can be used as part of the next HIFIS Annual Report. 


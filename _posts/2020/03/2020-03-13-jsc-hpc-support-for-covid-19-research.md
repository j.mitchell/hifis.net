---
title: "JSC: HPC support for COVID-19 research"
title_image: default
date: 2020-03-13
authors:
  - jandt
layout: blogpost
categories:
  - News
excerpt:
  The Jülich Supercomputing Center (JSC) offers computing time and support
  on its HPC facilities for research on the COVID-19 virus.
lang: en
lang_ref: 2020-03-13-jsc-hpc-support-for-covid-19-research
---

{{ page.excerpt }}

<a type="button" class="btn btn-outline-primary btn-lg" href="https://www.fz-juelich.de/SharedDocs/Meldungen/IAS/JSC/EN/2020/offer-covid-19.html">
  <i class="fas fa-external-link-alt"></i> Read more
</a>

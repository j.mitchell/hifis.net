---
date: 2019-10-17
title: Tasks in Oct 2019
service: overall
---

## HIFIS Conference

The [first conference on HIFIS](https://indico.desy.de/event/23411/timetable/#20191017) took place at DESY Hamburg.

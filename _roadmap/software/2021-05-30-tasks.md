---
date: 2021-05-30
title: Tasks in May 2021
service: software
---

## Evaluation of Helmholtz-wide Research Software Directory ([RSD](https://github.com/research-software-directory/research-software-directory/blob/master/README.md#what-is-it)) Prototype
HIFIS Community evaluates the reusability of the [Netherlands RSD](https://research-software.nl/) as RSD for all Helmholtz centers.

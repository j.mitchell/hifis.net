---
date: 2022-12-01
title: Tasks in December 2022
service: software
---

## Improving KPIs for Consultation

In 2021 the proposed goal of introducing a post-consulting survey
for KPI-measurement was achieved by the Consulting work package.
Therefore, this year the work package will focus on improving the KPIs
by analysing software projects of clients, measure potential improvements
and the long-term effects after a HIFIS consultation.

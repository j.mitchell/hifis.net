---
date: 2021-11-01
title: Tasks in Nov 2021
service: cloud
---

## Release of Helmholtz Cloud Portal
The production version of the Helmholtz Cloud Portal will be made available.
The new search function and filter options help to find a suitable service.
The availability of the service is tested automatically and is visible in the service card. The service description contains all main information such as support, service level and limitations.
User comments are welcome in the new free text field and will help to continuously improve the usability of the portal. 

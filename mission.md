---
title: HIFIS Mission
title_image: default
layout: default
excerpt:
    HIFIS Mission
---

# Our Aim

The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and
common access to data treasure and -services.
At the same time, the significance of a sustainable software development for the research process is recognised.

**HIFIS builds and sustains an excellent IT infrastructure connecting all Helmholtz research fields and centres.**

This includes the Helmholtz Cloud, where members of the Helmholtz Association of German Research Centres provide selected IT-Services for joint use. HIFIS further supports Research Software Engineering (RSE) with a high level of quality, visibility and sustainability.

The HIFIS clusters develop technologies, processes and policy frameworks for harmonized access to fundamental backbone services, such as Helmholtz AAI, and Helmholtz Cloud services. HIFIS Software also provides education, technology, consulting and community services.

**Helmholtz Digital Services for Science — Collaboration made easy.**

### Further Information

Almost all information on HIFIS is publicly available.

* [Publications]({% link publications.md %})
* [Media and Materials]({% link media/index.md %})
* [Technical and Administrative Documentation]({% link documentation.md %})
* Original [platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf)

### Contact

Contact us at <support@hifis.net> if you are missing any information, or if you have requests or comments.
